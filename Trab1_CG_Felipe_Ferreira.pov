// PoVRay 3.7 Scene File " ... .pov"
// author: Felipe Ferreira dos Santos


//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }}
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
#include "shapes3.inc"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {/*ultra_wide_angle*/    // front view
  location  <1.0 , 3.0 ,-6.0>
  right     x*image_width/image_height
  look_at   <1.0 , 2.0 , 0.0>
}
#declare Camera_1 = camera {/*ultra_wide_angle*/ angle 90   // diagonal view
  location  <3.0 , 3.5 ,-4.0>
  right     x*image_width/image_height
  look_at   <1.0 , 2.0 , 0.0>
}

// panoramic lens for wide field of view with less distortion
#declare Camera_2 = camera {
  panoramic
  location <1, 3, -3>                  // position
  look_at  <1, 2,  0>                  // view
  right    x*image_width/image_height  // aspect
  // angle    180                      // field (constant 180 degrees!!)
}

#declare Camera_3 = camera {/*ultra_wide_angle*/ angle 90        // top view
  location  <0.0 , 6.0 ,-0.001>
  right     x*image_width/image_height
  look_at   <2.0 , 1.0 , 0.0>
}
#declare Camera_4 = camera {/*ultra_wide_angle*/ angle 90 // left side view
  location  <1.0 , 1.0 , 4.0>
  right     x*image_width/image_height
  look_at   <1.0 , 2.0 , 0.0>
}
#declare Camera_7 = camera {/*ultra_wide_angle*/ angle 90 // left side view
  location  <1.0 , 0.5 , -4>
  right     x*image_width/image_height
  look_at   <1.0 , 3.0 , 0.0>
}
#declare Camera_5 = camera {/*ultra_wide_angle*/ angle 90 // left side view
  location  <-2.0 ,3.0 , 0.0>
  right     x*image_width/image_height
  look_at   <0.0 ,3.0 , 0.0>
}
#declare Camera_6 = camera {/*ultra_wide_angle*/ angle 90 // back side
  location  <1.0 ,3.0 , 5.0>
  right     x*image_width/image_height
  look_at   <1.0 ,3.0 , 0.0>
}

#declare Camera_8 = camera {/*ultra_wide_angle*/ angle 90 // back side
  location  <3.0 ,0.5 , -3.0>
  right     x*image_width/image_height
  look_at   <1.0 ,3.0 , 0.0>
}


camera{Camera_8}  

// sun ---------------------------------------------------------------------
light_source{<1500,2500,-2500> color White}
// sky ---------------------------------------------------------------------
plane{<0,1,0>,1 hollow
texture{ pigment{ bozo turbulence 0.76
  color_map { [0.5 rgb <0.20, 0.20, 1.0>]
    [0.6 rgb <1,1,1>]
    [1.0 rgb <0.5,0.5,0.5>]}
  }
  finish {ambient 1 diffuse 0} }
  scale 10000}
// fog ---------------------------------------------------------------------
  fog{fog_type   2
    distance   50
    color      White
    fog_offset 0.1
    fog_alt    2.0
    turbulence 0.8}
// ground ------------------------------------------------------------------
    plane { <0,1,0>, 0
      texture{ pigment{ color rgb<0.35,0.65,0.0>*0.9 }
      normal { bumps 0.75 scale 0.015 }
      finish { phong 0.1 }
} // end of texture                                                        
                                                                                                                                      
} // end of plane                       
  



//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
#declare box1 = isosurface {
  function{
      f_rounded_box(x,y,z,
      0.1, // radius of curvature
      0.5,1, 0.5// scale<x,y,z>
      )
  }

  threshold 0
  contained_by {box {<-0.5,-2,-0.5>,<0.5,2,0.5>}}
  max_gradient 3.168
  accuracy 0.0001
  evaluate 1,20,0.99
  texture {
      pigment{
        image_map{
           png "Texture_Yes_Man.png"
           map_type 0    // planar
           interpolate 2 // bilinear
         }
       }
    }
  scale 0.35
  rotate <0,0,0>
  rotate <0,0,0>
  translate < 0, 1, 0>
} // end of isosurface -------------------------------------------------------

#declare box2 = isosurface { //-------------------------------------------------------------
function{
  f_rounded_box(x,y,z,
0.1, // radius of curvature
0.4,0.98, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.5,-2,-0.2>,<0.5,2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {
  pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale 0.20
rotate <0,0,0>
rotate <0,0,0>
translate < 0, 0.9, -0.15>
}

#declare botaoMao1  = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,0,255>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < -0.035, 1.05, -0.165>
}

#declare botaoMao2 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < 0.04, 1.05, -0.165>
}
#declare botaoMao3 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < -0.035, 0.98, -0.165>
}

#declare botaoMao4 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,255,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < 0.04, 0.98, -0.165>
}

#declare botaoMao5 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,255,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < -0.035, 0.91, -0.165>
}

#declare botaoMao6 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,0,255>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < 0.04, 0.91, -0.165>
}

#declare botaoMao7 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,0,255>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < -0.035, 0.84, -0.165>
}

#declare botaoMao8 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < 0.04, 0.84, -0.165>
}

#declare botaoMao9 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < -0.035, 0.77, -0.165>
}

#declare botaoMao10 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,255,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.15
rotate <0,0,0>
rotate <0,0,0>
translate < 0.04, 0.77, -0.165>
}

#declare leftcy = cylinder { <0,0,0>,<0,2.0,0>,0.30
texture {Chrome_Metal pigment{ color rgb< 1, 1, 1>*0.25 } //  color Gray25
// normal { bumps 0.5 scale 0.05 }
finish { phong 1 reflection {0.04 metallic 0.20}}
} // end of texture
// end of texture
scale <0.05,0.18,0.05> rotate<0,0,0> translate<0.18,0.80,0>
} // end of cylinder  ------------------------------------

#declare torus1 = torus { 1.0,0.25
texture { Chrome_Metal
} // end of texture
scale <0.02,0.025,0.02> rotate<0,0,0> translate<0.17,1.16,0>
} // end of torus  -------------------------------
#declare torus2 = torus { 1.0,0.25
texture { Chrome_Metal
} // end of texture
scale <0.02,0.025,0.02> rotate<0,0,0> translate<0.17,0.8,0>
} // end of torus  -------------------------------

#declare finger11 = box { <0,0,0>,< 0.3, 0.5, 0.055>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<0,0,0> translate<-0.08,0.4,-0.17>
} // end of box --------------------------------------

#declare finger12 = box { <0,0,0>,< 0.3, 0.5, 0.04>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<-50,0,0> translate<-0.08,0.24,0.03>
} // end of box --------------------------------------

#declare eixofinger1 = cylinder { <-0.45,0,0>,<0.45,0,0>, 0.04
texture {Chrome_Metal pigment{ color rgb< 1, 1, 1>*0.25 } //  color Gray25
// normal { bumps 0.5 scale 0.05 }
finish { phong 1 reflection {0.04 metallic 0.20}}
} // end of texture
scale <0.17,0.2,0.2> rotate<0,0,0> translate<-0.008,0.4,-0.167>
} // end of cylinder  ------------------------------------

#declare finger21 = box { <0,0,0>,< 0.3, 0.5, 0.055>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<0,90,0> translate<-0.18,0.4,0.08>
} // end of box --------------------------------------

#declare finger22 = box { <0,0,0>,< 0.3, 0.5, 0.04>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<-35,90,0> translate<-0.03,0.18,0.08>
} // end of box --------------------------------------

#declare eixofinger2 = cylinder { <-0.45,0,0>,<0.45,0,0>, 0.04
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture e
scale <0.17,0.2,0.2> rotate<0,90,0> translate<-0.17,0.4,0.005>
} // end of cylinder  ------------------------------------

#declare finger31 = box { <0,0,0>,< 0.3, 0.5, 0.055>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // e texture nd of texture
scale <0.5,0.5,0.5> rotate<0,90,0> translate<-0.18,0.4,0.08>
} // end of box --------------------------------------

#declare finger32 = box { <0,0,0>,< 0.3, 0.5, 0.04>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<-35,90,0> translate<-0.03,0.18,0.08>
} // end of box --------------------------------------

#declare eixofinger3 = cylinder { <-0.45,0,0>,<0.45,0,0>, 0.04
texture {Chrome_Metal pigment{ color rgb< 1, 1, 1>*0.25 } //  color Gray25
// normal { bumps 0.5 scale 0.05 }
finish { phong 1 reflection {0.04 metallic 0.20}}
} // end of texture  of texture
scale <0.17,0.2,0.2> rotate<0,90,0> translate<-0.17,0.4,0.005>
} // end of cylinder  -----


#declare finger11dif = box { <0,0,0>,< 0.5, 0.5, 0.3>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<-20,90,0> translate<-0.15,0.1,0.1>
} //
#declare finger12dif = box { <0,0,0>,< 0.5, 0.5, 0.3>
texture {  pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture
scale <0.5,0.5,0.5> rotate<-20,-90,0> translate<0.135,0.1,-0.1>
} //


#declare finger21dif = box { <0,0,0>,< 0.7, 0.5, 0.4>
texture {  pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture exture
scale <0.5,0.5,0.5> rotate<-20,0,0> translate<-0.2,0.12,-0.2>
}

#declare finger22dif = box { <0,0,0>,< 0.7, 0.5, 0.4>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of texture f texture
scale <0.5,0.5,0.5> rotate<20,0,0> translate<-0.2,0.12,0>
}
#declare antarmleft =  cylinder{ <0,0.0,0>,<0,0.55,0>, 0.15
open
texture { Chrome_Metal pigment{ color rgb<0.168,0.168,0.168>*1.1}
normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
finish { phong 0.4 reflection{ 0.05 metallic 0.10} }
} // end of texture

scale <1,1,1> rotate<0,0,0> translate<0,1.36,0>
} // end of cylinder -------------------------------------
#declare lastarmleft = cylinder{ <0,0.0,0>,<0,0.6,0>, 0.15
open
texture { Chrome_Metal pigment{ color rgb<0.168,0.168,0.168>*1.1}
normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
finish { phong 0.4 reflection{ 0.05 metallic 0.10} }
} // end of texture

scale <1,1,1> rotate<0,0,-17> translate<0,1.9,0>
} // e
#declare cutuveloleft = cylinder{ <0,0.0,0>,<0,0.16,0>, 0.14
open
texture { Chrome_Metal pigment{ color rgb<0.168,0.168,0.168>*1.1}
normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
finish { phong 0.4 reflection{ 0.05 metallic 0.10} }
} // end of texture

scale <1,1,1> rotate<0,0,-17> translate<-0.026,1.85,-0>
} // e

#declare acessorie1left = cylinder{ <0,0.0,0>,<0,0.05,0>, 0.16
open
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textur

scale <1,1,1> rotate<0,0,0> translate<-0,1.45,0>
}
#declare shouderleft = lathe{  // rotates a 2-D outline of points around the Y axis to create a 3-D shape
linear_spline //  linear_spline | quadratic_spline | cubic_spline
12,      // number of points,
<2.0, 1.0>, // list of <x,y> points,
<2.0,-1.0>,
<3.0,-1.0>,
<3.4,-2.0>,
<4.0,-1.1>,
<3.6,-0.9>,
<2.6, 0.0>,
<3.6, 0.9>,
<4.0, 1.1>,
<3.4, 2.0>,
<3.0, 1.0>,
<2.0, 1.0>
// sturm
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale<1,1,1>*0.08
rotate<0,0,-25>
translate<0.2,2.4,0>
} // ----------------------------------------------- end of lathe object

#declare shouderright = lathe{  // rotates a 2-D outline of points around the Y axis to create a 3-D shape
linear_spline //  linear_spline | quadratic_spline | cubic_spline
12,      // number of points,
<2.0, 1.0>, // list of <x,y> points,
<2.0,-1.0>,
<3.0,-1.0>,
<3.4,-2.0>,
<4.0,-1.1>,
<3.6,-0.9>,
<2.6, 0.0>,
<3.6, 0.9>,
<4.0, 1.1>,
<3.4, 2.0>,
<3.0, 1.0>,
<2.0, 1.0>
// sturm
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale<1,1,1>*0.08
rotate<0,0,25>
translate<2,2.35,0>
} // ----------------------------------------------- end of lathe object-------


#declare accesories1right = lathe{  // rotates a 2-D outline of points around the Y axis to create a 3-D shape
linear_spline //  linear_spline | quadratic_spline | cubic_spline
2,      // number of points,
<3.0,-1.0>,
<3.4,-2.0>
// sturm
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale<1,1,1>*0.06
rotate<0,0,0>
translate<2.2,1.45,0>
}
#declare wheellip =  superellipsoid{ <1.00,0.25>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textur

scale <0.55,0.6,0.35>
rotate<0,90,0>
translate<1.2,0.60,0>
} // ----------------- end superellipsoid
#declare wheelblack = cylinder { <-1,0,0>,<1,0,0>, 0.60
texture {pigment{ color rgb<1,1,1>*0.05}
//normal  { bumps 0.5 scale <0.25, 0.005,0.005>}
finish  { phong 0.1  }
} // end of texture
scale <0.27,1,1> rotate<0,0,0> translate<1.2,0.60,0>
} // end of cylinder  ------------------------------------


#declare suporteinicial = object{ Supertorus( 1.50, 0.25, // Radius_Major, Radius_Minor,
0.25, 1.45, // Major_Control, Minor_Control,
0.001, 1.50) // Accuracy, Max_Gradient)

texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textu
scale <0.3,0.3,0.2>
rotate<0,0,0>
translate<1.2,1.4,0>
} //----------------------------------------------------
#declare mergesuporte = box { <-1.00, 0.00, -1.00>,< 1.00, 0.20, 1.00>

texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textu texture

scale <0.45,0.45,0.3> rotate<0,0,0> translate<1.2,1.37,0>
} // end of box --------------------------------------
#declare cortesuporte = box { <-1.00, 0.00, -1.00>,< 1.00, 0.20, 1.00>

texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textu texture

scale <0.6,0.45,0.6> rotate<0,0,0> translate<1.2,1.31,0>
} // end of box --------------------------------------



//----------------------------------------------------------------------------
//      Column_N(  N, Radius, Height )
#declare screw1  = object{ Column_N( 10,    0.15,   0.15 )
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.5,0.5,0.5> rotate<0,0,90> translate<0.85,0.63,0>
} // end of object -----------------------------------------------------
#declare screw2  = object{ Column_N( 10,    0.15,   0.15 )
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.5,0.5,0.5> rotate<0,0,90> translate<1.64,0.63,0>
} // end of object -----------------------------------------------------
#declare rodela1 = sphere { <0,0,0>, 0.12
scale<1.8,0.22,1>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textu texture

rotate<0,0,90>  translate<0.81,0.72,0>
}  // end of sphere --------------------------------------------------
#declare rodela2 = sphere { <0,0,0>, 0.12
scale<1.8,0.22,1>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textu texture

rotate<0,0,90>  translate<1.612,0.72,0>
}  // end of sphere --------------------------------------------------


#declare supleft =  superellipsoid{ <0.2,0.2>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end re
scale <0.015,0.2,0.1>
rotate<0,0,0>
translate<0.8,1,0>
} // -------------- end superellipsoid

#declare sup2right =  superellipsoid{ <0.2,0.2>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end re
scale <0.015,0.1,0.23>
rotate<0,0,0>
translate<1.6,1.3,0>
} // -------------- end superellipsoid

#declare supright =  superellipsoid{ <0.2,0.2>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end re
scale <0.015,0.2,0.1>
rotate<0,0,0>
translate<1.6,1,0>
} // -------------- end superellipsoid

#declare sup2left =  superellipsoid{ <0.2,0.2>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end re
scale <0.015,0.1,0.23>
rotate<0,0,0>
translate<0.8,1.3,0>
}

#declare torsodown = superellipsoid{ <1.00,0.25>
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end re
scale <0.2,0.2,0.2>
rotate<90,0,0>
translate<1.2,1.55,0>
} // ----------------- end superellipsoid
#declare accestorso = cylinder { <-1,0,0>,<1,0,0>, 0.05
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of textureure
scale <0.05,1,1> rotate<0,0,0> translate<1.38,1.6,0>
} // end of cylinder  ------------------------------------
#declare accestorso2 = cylinder { <-1,0,0>,<1,0,0>, 0.05
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.05,1,1> rotate<0,0,0> translate<1,1.6,0>
} // end of cylinder  ------------------------------------
#declare accestorso3 = cylinder { <0,0,0>,<0,2.00,0>, 0.05
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture

scale <1,0.05,1> rotate<90,0,0> translate<1.20,1.6,-0.235>
} // end of cylinder -------------------------------------
#declare accestorso4 = cylinder { <0,0,0>,<0,2.00,0>, 0.05
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <1,0.05,1> rotate<90,0,0> translate<1.20,1.6,0.135>
} // end of cylinder -------------------------------------

#declare cabo1 = object{ Segment_of_Torus ( 1.00, // radius major,
0.25, // radius minor,
-200  // segment angle (in degrees)
) //-----------------------------------
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.08,0.03,0.06> rotate<90,0,-60>  translate<0.92,1.68,0>
} // end of Torus_Segment(...) ---------------------------
//--------------------------------------------------------------------------
#declare cabo2 = object{ Segment_of_Torus ( 1.00, // radius major,
0.25, // radius minor,
-200  // segment angle (in degrees)
)
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.08,0.03,0.06> rotate<90,0,80>  translate<1.45,1.68,0>
} // end of Torus_Segment(...) ---------------------------
//-------------------------------------------------------------------------

#declare cabo3 = object{ Segment_of_Torus ( 1.00, // radius major,
0.25, // radius minor,
-200  // segment angle (in degrees)
) //-----------------------------------
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.08,0.03,0.06> rotate<0,-25,90>  translate<1.20,1.68,0.2>
} // end of Torus_Segment(...) ---1------------------------
//-------------

#declare cabo4 = object{ Segment_of_Torus ( 1.00, // radius major,
0.25, // radius minor,
-200  // segment angle (in degrees)
)
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of textureure

scale <0.08,0.03,0.06> rotate<0,220,90>  translate<1.20,1.68,-0.2>
} // end of Torus_Segment(...) ---1------------------------
//-------------
#declare torsomiddle = cylinder { <0,0,0>,<0,1.00,0>, 0.45

texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end re

scale <1,0.37,1> rotate<0,0,0> translate<1.2,1.72,0>
} // end of cylinder -------------------------------------

#declare torsotop = cylinder{ <0,0.0,0>,<0,0.75,0>, 0.65
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture

scale <1,0.5,1> rotate<0,0,0> translate<1.2,2,0>
} // end of cylinder -------------------------------------
//---------------------------------------------
#declare accesmiddletorso1 = object{ // Round_Box(A, B, WireRadius, UseMerge)
Round_Box(<-1,0,-1>,<1,1,1>, 0.25, 0)

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.04 metallic 0.75} }
} // end nd of texture
scale <0.05,0.17,0.15> rotate<0, 0,0> translate<0.65,1.76,0>
} // ---------------------------------------------------------

#declare accesmiddletorso2 = object{ // Round_Box(A, B, WireRadius, UseMerge)
Round_Box(<-1,0,-1>,<1,1,1>, 0.25, 0)

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.04 metallic 0.75} }
} // end nd of texture
scale <0.05,0.17,0.15> rotate<0, 0,0> translate<1.75,1.76,0>
} // ---------------------------------------------------------
#declare accesmiddletorso3 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.04 metallic 0.75} }
} // etexture

scale <0.08,0.017,0.08> rotate<0,0,0> translate<0.68,1.8,0>
} // end of box --------------------------------------

#declare accesmiddletorso4 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.04 metallic 0.75} }
} // etexture

scale <0.08,0.017,0.08> rotate<0,0,0> translate<0.68,1.85,0>
} // e
#declare accesmiddletorso5 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.04 metallic 0.75} }
} // etexture

scale <0.08,0.017,0.08> rotate<0,0,0> translate<1.7,1.8,0>
} // end of box --------------------------------------

#declare accesmiddletorso6 =  box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.04 metallic 0.75} }
} // etexture

scale <0.08,0.017,0.08> rotate<0,0,0> translate<1.7,1.85,0>
} // e

#declare painel1 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

    texture {
      pigment{
         color rgb< 1, 1, 1>*0.25
         } //  color Gray25
      finish {
        phong 1 reflection {
          0.04 metallic 0.20
          }
        }
    } // end of texture

    scale <0.13,0.057,0.03> rotate<0,0,0> translate<1.2,2.1,-0.63>
}


#declare painel2 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

  texture {
    pigment{
      image_map{
         png "Texture_Yes_Man.png"
         map_type 0    // planar
         interpolate 2 // bilinear
       }
     }
  } // end of textur of texture

  scale <0.15,0.15,0.03> rotate<0,0,0> translate<1.2,2.07,-0.625>
}

#declare painelscrew1 = object{ Column_N( 10,    0.15,   0.15 )
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.22,0.22,0.22> rotate<90,0,0> translate<1.12,2.27,-0.68>
} // end of object ---------------------------------------------
#declare painelscrew2 = object{ Column_N( 10,    0.15,   0.15 )
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of texture
scale <0.22,0.22,0.22> rotate<90,0,0> translate<1.28,2.27,-0.68>
} // end of ob

#declare painelbutton = isosurface { //-------------------------------------------------------------
function{
  f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.3, 2.127, -0.64>
}
#declare painelbutton1 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,0,255>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.3, 2.18, -0.64>
}
#declare painelbutton2 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.25, 2.18, -0.64>
}

#declare painelbutton3 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,255,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.25, 2.127, -0.64>
}


#declare painelbutton4 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,0,255>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.20, 2.127, -0.64>
}


#declare painelbutton5 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.15, 2.127, -0.64>
}

#declare painelbutton6 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,255,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.10, 2.127, -0.64>
}

#declare painelbutton7 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<255,0,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.10, 2.18, -0.64>
}

#declare painelbutton8 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,0,255>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.15, 2.18, -0.64>
}

#declare painelbutton9 = isosurface { //-------------------------------------------------------------
function{
f_rounded_box(x,y,z,
0.1, // radius of curvature
0.2,0.2, 0.4// scale<x,y,z>
)
}

threshold 0
contained_by {box {<-0.2,-0.2,-0.2>,<0.2,0.2,0.2>}}
max_gradient 3.168
accuracy 0.0001
//evaluate 1,20,0.99
texture {pigment{ color rgb<0,255,0>}
//normal {bumps 0.5 scale 0.05}
finish { phong 1 reflection {0.04 metallic 0.20}}
}
//normal {bumps 0.5 scale 0.05}
scale 0.11
rotate <0,0,0>
rotate <0,0,0>
translate < 1.20, 2.18, -0.64>
}

#declare headprinc = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

texture {
pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end of textur of texture
scale <0.65,0.5,0.56> rotate<0,0,0> translate<1.2,2.4,0>
} // end of box --------------------------------------

// linear prism in x-direction: from ... to ..., number of points (first = last)
#declare painelprism1 = prism { -1.00 ,1.00 , 4
<-1.00, 0.00>, // first point
< 1.00, 0.00>,
< 0.00, 1.50>,
<-1.00, 0.00>  // last point = first point!!!
rotate<-90,-90,0> //turns prism in x direction! Don't change this line!


texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end rend of texture

scale <0.05, 0.20, 0.2>
rotate<0,90,55>
translate<1.2, 2.228, -0.605>
}
#declare painelprism2 = prism { -1.00 ,1.00 , 4
<-1.00, 0.00>, // first point
< 1.00, 0.00>,
< 0.00, 1.50>,
<-1.00, 0.00>  // last point = first point!!!
rotate<-90,-90,0> //turns prism in x direction! Don't change this line!


texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end rend of texture

scale <0.05, 0.20, 0.2>
rotate<0,90,-55>
translate<1.185, 2.228, -0.6>
} // end of prism --------------------------------------------------------
#declare headcurv1 = cylinder { <0,0,0>,<0,0.68,0>,0.20
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end rendf texture
scale <1,1,1> rotate<0,0,0> translate<0.6,2.72,-0.35>
} // end of cylinder  ------------------------------------

#declare headcurv2 = cylinder { <0,0,0>,<0,0.68,0>,0.20
texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end rendf texture
scale <1,1,1> rotate<0,0,0> translate<1.8,2.72,-0.35>
} // end of cylinder  ------------------------------------


#declare shoulderleftprim = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

    texture {
      pigment{
        image_map{
           png "Texture_Yes_Man.png"
           map_type 0    // planar
           interpolate 2 // bilinear
          //
        }
      }
    } // end rend of texture

    scale <0.5,0.42,0.44> rotate<0,0,0> translate<0.05,2.7,0>
} // end of box --------------------------------------
#declare shoulderrightprim = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

    texture {
      pigment{
        image_map{
          png "Texture_Yes_Man.png"
          map_type 0    // planar
          interpolate 2 // bilinear
          //
        }
      }
    } // end rend of texture

    scale <0.5,0.42,0.44> rotate<0,0,0> translate<2.35,2.7,0>
} // end of box --------------------------------------

#declare headtop = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

texture {pigment{image_map{ png "Texture_Yes_Man.png"
map_type 0    // planar
interpolate 2 // bilinear
//
}
}
} // end rend of texture

scale <0.5,0.12,0.2> rotate<0,0,0> translate<1.2,3.2,-0.41>
} // end of box --------------------------------------
#declare partant1 = cylinder { <0,0,0>,<0,0.10,0>, 0.02

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
//normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of tex
scale <1,1,1> rotate<0,0,0> translate<0.9,3.385,-0.54>
} // end of cylinder -------------------------------------
#declare partant2 = cylinder { <0,0,0>,<0,0.30,0>, 0.01

texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
//normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of tex of texture

scale <1,1,1> rotate<0,0,0> translate<0.9,3.385,-0.54>
} // end of cylinder -------------------------------------
#declare partant3 = torus { 1,0.1
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
//normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of tex
scale <0.1,0.1,0.1> rotate<90,0,0> translate<0.89,3.79,-0.55>
} // end of torus  -------------------------------
#declare speaker1 = torus { 1.3,0.1
texture {  Chrome_Metal
  pigment{ color rgb<0.94,0.93,0.75>*0.25}
//normal { ripples 3.5 scale 0.05  translate<0,-1.0,0> }
  finish { phong 1 reflection { 0.3 metallic 0.75} }
} // end of tex
scale <0.1,0.1,0.1> rotate<90,0,0> translate<1.5,3.4,-0.63>
} // end of torus  -------------------------------

#declare speaker2 = cylinder { <0,0,0>,<0,0,0.040>,0.135

  texture {
     pigment{
       color rgb<0.94,0.93,0.75>*0.25
     }
    finish {
      phong 1 reflection {
         0.3 metallic 0.75
         }
      }
  }
  scale <1,1,1> rotate<0,0,0> translate<1.5,3.4,-0.635>
} // end of cylinder  ------------------------------------

#declare speaker3 = cylinder { <0,0,0>,<0,0,0.040>,0.05

    texture {
      Chrome_Metal pigment{
         color rgb<0.94,0.93,0.75>*0.25
       }
      finish {
        phong 1 reflection {
           0.3 metallic 0.75
           }
        }
    }

  scale <1,1,1> rotate<0,0,0> translate<1.5,3.4,-0.64>
} // end of cylinder  ------------------------------------

#declare speaker4 = superellipsoid{ <0.25,0.25>

    texture {
      Chrome_Metal pigment{
         color rgb<0.94,0.93,0.75>*0.25
       }
      finish {
        phong 1 reflection {
           0.3 metallic 0.75
           }
         }
    }

    scale <0.17,0.09,0.2>
    rotate<0,0,0>
    translate<0.95,3.325,-0.42>
} // -------------- end superellipsoid

#declare speaker5 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

    texture {
      Chrome_Metal pigment{
         color rgb< 0.0, 0.0, 0.5>
      } //  dark blue

    finish {
      phong 1 reflection {
        0.04 metallic 0.75
        }
      }
    } // end rend of texture
    scale <0.02,0.04,0.1> rotate<0,0,0> translate<0.88,3.3,-0.5205>
}

#declare speaker6 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

  texture {
    Chrome_Metal pigment{
       color rgb< 0.0, 0.0, 0.5>
    } //  dark blue

  finish {
    phong 1 reflection {
      0.04 metallic 0.75
      }
    }
  } // end rend of texture

  scale <0.02,0.04,0.1> rotate<0,0,0> translate<1.02,3.3,-0.5205>
}

#declare speaker7 = box { <-1.00, 0.00, -1.00>,< 1.00, 2.00, 1.00>

    texture {
      Chrome_Metal pigment{
         color rgb< 0.0, 0.0, 0.5>
      } //  dark blue
      finish {
        phong 1 reflection {
          0.04 metallic 0.75
        }
     }
    } // end rend of texture

    scale <0.02,0.04,0.1> rotate<0,0,0> translate<0.95,3.3,-0.5205>
}


//----------------------------------------------------------------------------
#declare facearound = object{ Half_Hollowed_Rounded_Cylinder2( //
  3.1,//  Len_total, // total_Lenght from end to end
  0.9,//  outer radius
  0.25,// corner radius < outer radius
  0.10,// border radius < (corner radius)/2
  1, //  border scale y ( >0 )  0 = no rounded borders!
  1  //  Merge_On , // 0 = union, 1 = merge !
  ) //--------------------------------------------
  texture {
    Chrome_Metal
      pigment{
        color rgb<0.94,0.93,0.75>*0.25
      }
      finish {
        phong 1 reflection {
          0.3 metallic 0.75
          }
      }
  } // end of texture
  scale<0.34,0.34,0.34>
  translate <-1.2,0.56,-2.8>
  rotate<-90,0,180>
} // end of object -----------------------------------------------------
//----------------------------------------------------------------------------
#declare face = box { <0, 0.00, 0>,< 1, 1, 1>

  texture {
    pigment{
      image_map{
        png "Yes_Face.png"
        map_type 0    // planar
        interpolate 4 // bilinear
        once //
      }
    }
  } // end of texture

  scale <0.8,0.5,0.3> rotate<0,0,0> translate<0.8,2.55,-0.58>
} // end of box -----------------

#declare shoulderlcut1 = box { <0.00, 0.00, 0.00>,< 1.00, 1.00, 1.00>

    texture {
      pigment{
        image_map{
          png "Texture_Yes_Man.png"
          map_type 0    // planar
          interpolate 4 // bilinear
          once //
        }
      }
    } // end of texture

    scale <0.5,1,2> rotate<0,0,45> translate<0.6,3.35,-0.45>
} // end of box --------------------------------------

  #declare shoulderlcut2 = box { <0.00, 0.00, 0.00>,< 1.00, 1.00, 1.00>
    texture {
      pigment{
        image_map{
          png "Texture_Yes_Man.png"
          map_type 0    // planar
          interpolate 4 // bilinear
          once //
        }
      }
    } // end of texture

    scale <0.5,1,2> rotate<0,0,-45> translate<1.48,3.75,-0.45>
} // end of box --------------------------------------


 #declare roundcutleft = object{ Supertorus(
   1.00, 0.25, // Radius_Major, Radius_Minor,
   1.00, 0.45, // Major_Control, Minor_Control,
   0.001, 1.50) // Accuracy, Max_Gradient)

   texture {
     pigment{
       image_map{
         png "Texture_Yes_Man.png"
         map_type 1    // planar
         interpolate 4 // bilinear
         once //
       }
     }
   } // end of texture

  scale <1,3,1>
  rotate<90,0,0>
  translate<0.27,3,0>
} //----------------------------------------------------

#declare accesoleftshoulder = object{ // Round_Box(A, B, WireRadius, UseMerge)
        Round_Box(<-1,0,-1>,<1,1,1>, 0.25   , 0)

      texture {
        Chrome_Metal pigment{
           color rgb<0.94,0.93,0.75>*0.25
         }
        finish {
          phong 1 reflection {
             0.3 metallic 0.75
             }
           }
      }
      scale<0.035,0.5,0.1>  rotate<0, 0,0> translate<0.29,2.9,-0.36>
    } // ---------------------------------------------------------
#declare accesorightshoulder = object{ // Round_Box(A, B, WireRadius, UseMerge)
        Round_Box(<-1,0,-1>,<1,1,1>, 0.25   , 0)

      texture {
        Chrome_Metal pigment{
           color rgb<0.94,0.93,0.75>*0.25
         }
        finish {
          phong 1 reflection {
             0.3 metallic 0.75
             }
           }
      }
      scale<0.035,0.5,0.1>  rotate<0, 0,0> translate<2.05,2.9,-0.36>
    } // ---------------------------------------------------------

#declare starleft1 = object{ // Round_Box(A, B, WireRadius, UseMerge)
        Round_Box(<-1,0,-1>,<1,1,1>, 0.25   , 0)

      texture {
        Chrome_Metal pigment{
           color rgb<0.94,0.93,0.75>*0.25
         }
        finish {
          phong 1 reflection {
             0.3 metallic 0.75
             }
           }
      }
      scale<0.005,0.3,0.1>  rotate<0,0,45> translate<-0.1,3.2,-0.35>
    } // ---------------------------------------------------------
#declare starleft2 = object{ // Round_Box(A, B, WireRadius, UseMerge)
        Round_Box(<-1,0,-1>,<1,1,1>, 0.25   , 0)

      texture {
        Chrome_Metal pigment{
           color rgb<0.94,0.93,0.75>*0.25
         }
        finish {
          phong 1 reflection {
             0.3 metallic 0.75
             }
           }
      }
      scale<0.005,0.3,0.1>  rotate<0,0,-45> translate<0,3.2,-0.35>
    } // ---------------------------------------------------------
#declare starleft3 = object{ // Round_Box(A, B, WireRadius, UseMerge)
        Round_Box(<-1,0,-1>,<1,1,1>, 0.25   , 0)

      texture {
        Chrome_Metal pigment{
           color rgb<0.94,0.93,0.75>*0.25
         }
        finish {
          phong 1 reflection {
             0.3 metallic 0.75
             }
           }
      }
      scale<0.005,0.3,0.1>  rotate<0,0,-45> translate<-0.33,2.9,-0.35>
    } // --------
#declare starleft4 = object{ // Round_Box(A, B, WireRadius, UseMerge)
        Round_Box(<-1,0,-1>,<1,1,1>, 0.25   , 0)

      texture {
        Chrome_Metal pigment{
           color rgb<0.94,0.93,0.75>*0.25
         }
        finish {
          phong 1 reflection {
             0.3 metallic 0.75
             }
           }
      }
      scale<0.005,0.3,0.1>  rotate<0,0,45> translate<0.22,2.9,-0.35>
    } // --------

#declare starleft = union{
  object{starleft1}
  object{starleft2}
  object{starleft3}
  object{starleft4}
}

#declare shoudercutt = difference{
  object{shoulderleftprim}
  object{shoulderlcut1}
  object{roundcutleft}
}

#declare shoulderrightprimc = union{
  object{shoudercutt}
}

#declare shoudercutt2 = difference{
  object{shoulderrightprimc}
  object{shoulderlcut2}

  rotate<0,180,0>
  translate<2.33,0,0>
}

#declare starright = union{
  object{starleft}

  translate <2.45,0,0>
}

#declare shoulderleft = union{
  object{shoudercutt}
  object{accesoleftshoulder}
  object{starleft}
}

#declare shoulderright= union{
  object{shoudercutt2}
  object{accesorightshoulder}
  object{starright}
}

#declare head = union{
  object{headprinc}
  object{headcurv1}
  object{headcurv2}
  object{headtop}
  object{partant1}
  object{partant2}
  object{partant3}
  object{speaker1}
  object{speaker2}
  object{speaker3}
  object{speaker4}
  object{speaker5}
  object{speaker6}
  object{speaker7}
  object{face}
  object{facearound}
}

#declare painel = union{
  object{painel1}
  object{painel2}
  object{painelbutton}
  object{painelbutton1}
  object{painelbutton2}
  object{painelbutton3}
  object{painelbutton4}
  object{painelbutton5}
  object{painelbutton6}
  object{painelbutton7}
  object{painelbutton8}
  object{painelbutton9}
  object{painelscrew2}
  object{painelscrew1}
  object{painelprism1}
  object{painelprism2}
}

#declare torso = union{
  object{accesmiddletorso1}
  object{accesmiddletorso2}
  object{accesmiddletorso3}
  object{accesmiddletorso4}
  object{accesmiddletorso5}
  object{accesmiddletorso6}
  object{torsotop}
  object{torsomiddle}
  object{torsodown}
  object{accestorso}
  object{accestorso2}
  object{accestorso3}
  object{accestorso4}
  object{cabo1}
  object{cabo2}
  object{cabo3}
  object{cabo4}
  object{painel}
}


#declare suportecorte1 = difference{
  object{suporteinicial}
  object{cortesuporte}
}

#declare screw = merge{
  object{screw1}
  object{screw2}
}
#declare suportefinal = merge{
  object{suportecorte1}
  object{mergesuporte}
  scale<0.95,0.95,0.95>
  translate<0.08,0,0>
}

#declare suporteleft = union{
  object{rodela1}
  object{supleft}
  object{sup2left}
}
#declare suporteright = union{
  object{rodela2}
  object{supright}
  object{sup2right}
}
//------------------------------------------------------------------------------


#declare leftcylinder = merge{
  object{leftcy}
  object{torus1}
  object{torus2}
}

#declare finger1cut = difference{
  object{finger12}
  object{finger11dif}
  object{finger12dif}
}

#declare finger2cut = difference{
  object{finger22}
  object{finger21dif}
  object{finger22dif}
}

#declare finger3cut = difference{
  object{finger32}
  object{finger21dif}
  object{finger22dif}
}

#declare finger1 = merge{
  object{finger11}
  object{eixofinger1}
  //object{finger12}
  //object{finger11dif}
  object{finger1cut}
  //object{finger12dif}
}

#declare finger2 = merge{
  object{finger21}
  object{eixofinger2}
  //object{finger22}
  //object{finger21dif}
  //object{finger22dif}
  object{finger2cut}
}

#declare finger3 = merge{
  object{finger31}
  object{eixofinger3}
  //object{finger32}
  object{finger3cut}
  rotate<0,90,0>
  translate<0,0,-0.01>
}

#declare armyleft = union{
  object{antarmleft}
  object{lastarmleft}
  object{cutuveloleft}
}

#declare armyright = union{
  object{armyleft}
  rotate<0,180,0>
  translate<2.2,-0.15,0>
}

#declare lefthand = merge{
  object{box1}
  object{box2}
  object{botaoMao1}
  object{botaoMao2}
  object{botaoMao3}
  object{botaoMao4}
  object{botaoMao5}
  object{botaoMao6}
  object{botaoMao7}
  object{botaoMao8}
  object{botaoMao9}
  object{botaoMao10}
  object{leftcylinder}
  object{finger1}
  object{finger2}
  object{finger3}
  object{armyleft}
  object{acessorie1left}
  object{shouderleft}
  translate<-0,0.25,0>
}

#declare acessoriesright = union{
  object{acessorie1left}
  translate<2.2,-0.18,0>
}

#declare fingersright = union{
  object{finger1}
  object{finger2}
  object{finger3}
  rotate<0,200,0>
  translate<2.15,-0.09,0>
}

#declare righthand = merge{
  object{armyright}
  object{ Round_Cylinder_Tube( <0,-0,-0>, // starting point
    <2.2,0.2,-0.2>, // end point
    0.85, // major radius
    0.12, // minor radius (borders)
    1,  //  1 = filled; 0 = open tube
    1 // 0 = union, 1 = merge for transparent materials
  )texture {
      pigment{
        image_map{
          png "Texture_Yes_Man.png"
          map_type 0    // planar
          interpolate 2 // bilinear
          //
        }
      }
   } // end of textur of texture
    scale <0.29,0.29,0.29>
    rotate<0,-05,85>
    translate<2.17,0.57,0>
  }
  object{acessoriesright}
  object{accesories1right}
  object{shouderright}
  object{fingersright}
  translate<0.15,0.25,-0.05>
}

#declare legs = union{
  object{wheellip}
  object{wheelblack}
  object{screw}
  object{suporteleft}
  object{suporteright}
  object{torso}
}

union{
  object{lefthand}
  object{righthand}
  object{legs}
  object{suportefinal}
  object{shoulderleft}
  object{shoulderright}
  object{head}

  rotate<0,0,0>
}
